/* Esta Clase declara:
 *                      Atributos:  el Nombre del Tipo de Vehículo [dato tipo String]
 *                                  la Cantidad de Plazas que ocupa el Tipo de Vehículo [dato tipo int]
 * 
 *                      Métodos:    Constructor vacío
 *                                  Constructor con todos los Atributos
 * 
 *                                  ss que determina el Tipo de Vehículo (Atributo: nombre) [dato tipo String]
 *                                  Getter y Setter
 *                                  toString()
 *                                  equals() y hashCode()
 *                                  
 */
package Cochera;

import java.util.Objects;

/**
 *
 * @author vhgav
 */

    // declaro Clase TipoVehiculo
public class TipoVehiculo {

    // declaro Atributos de la Clsae
    int plazasQueOcupa;
    String nombre;

    // declaro Constructor vacío
    public TipoVehiculo() {
    }

    // declaro Constructor con todos los Atributos    
    public TipoVehiculo(int ancho, String medio_locomocion) {
        this.plazasQueOcupa = ancho;
        this.nombre = medio_locomocion;
    }
    
    // Método que determina el Tipo de Vehículo

    
    // declaro Getter y Setter
    public int getPlazasQueOcupa() {
        return this.plazasQueOcupa;
    }

    public void setPlazasQueOcupa(int cantPzas) {
        this.plazasQueOcupa = cantPzas;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String tipo_vehiculo) {
        this.nombre = tipo_vehiculo;
    }
    
    // Como buena práctica se declara el toString (cuando están determinados TODOS los atributos) 
    @Override
    public String toString() {
        return "TipoVehiculo{" + "plazasQueOcupa=" + plazasQueOcupa + ", nombre=" + nombre + '}';
    }

    // Como buena práctica se declaran equals (y hashCode) (cuando están determinados TODOS los atributos)    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.plazasQueOcupa;
        hash = 89 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoVehiculo other = (TipoVehiculo) obj;
        if (this.plazasQueOcupa != other.plazasQueOcupa) {
            return false;
        }
        return Objects.equals(this.nombre, other.nombre);
    }
}
