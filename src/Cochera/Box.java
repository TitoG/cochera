/* Esta Clase declara:
 *                     Atributos:   el Nombre (identificación) del Box [dato tipo String]
 *                                  la Cantidad de Plazas que tiene el Box [dato tipo int]
 *                                  el Estado (true: libre / false: ocupado) del Box [dato tipo boolean]
 *                                  el Vehículo que ocupa el (estaciona en el) Box [dato tipo Vehiculo]
 * 
 *           Variable de Clase:     ArrayList<>: Lista de los Boxes de la Cochera [dato tipo ArrayList<>]
 * 
 *                     Métodos:     Constructor vacío
 *                                  Constructor con los Atributos: nombre, cantPlazas
 *                                  Constructor con todos los Atributos
 *                                  
 *                                  Getter y Setter
 *                                  toString()
 *                                  equals() y hashCode()                       
 */
package Cochera;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author vhgav
 */
// declaro Clase Box
public class Box {

    // declaro Atributos de la Clase
    String nombre;
    int cantPlazas;
    //Vehiculo vehiculoEnBox;
    //boolean estado;
    //public static ArrayList<Box> boxesCochera = new ArrayList<Box>();       // variable de Clase
    ArrayList<Vehiculo> listaVehiculosEnBox;

    // declaro Constructor vacío
    public Box() {
    }

    // declaro Constructor con los Atributos: nombre, cantPlazas    
    public Box(String denominacion, int cantidadDePlazas) {
        this.listaVehiculosEnBox = new ArrayList<Vehiculo>();
        this.nombre = denominacion;
        this.cantPlazas = cantidadDePlazas;
    }

    // declaro Constructor con todos los Atributos
//    public Box(String denominacion, int cantidadDePlazas, Vehiculo vehiculoEstacionado, boolean condicion) {
//        this.listaVehiculosEnBox = new ArrayList<Vehiculo>();
//        this.nombre = denominacion;
//        this.cantPlazas = cantidadDePlazas;
//        //this.vehiculoEnBox = vehiculoEstacionado;
//        this.estado = condicion;
//    }
    // mostrar Lista de Vehículos
    public void mostrarListaVehiculosEnBox() {
        if (listaVehiculosEnBox.isEmpty()) {
            System.out.println("Box vacío");
        } else {
            for (int i = 0; i < listaVehiculosEnBox.size(); i++) {
                System.out.println(listaVehiculosEnBox.get(i));
            }
        }

    }

    // mostrar espacios disponibles en cada box
    // agregar vehiculos a la Lista de Vehículos
    public void agregarVehiculoaLista(Vehiculo vehiculo) {
        if (verificarDisponibilidadBox(vehiculo) >= vehiculo.getPlazasQueOcupa()) {
            listaVehiculosEnBox.add(vehiculo);
        }
    }
    //recorrer lista, verificar plazasQueOcupa el TipoVehiculo del vehiculo estacionado (si lo hay), sumarlo 
    //a plazasQueOcupa el TipoVehiculo del vehículo a ingresar debe ser <= cantPlazas (del Box)

    public int verificarDisponibilidadBox(Vehiculo vehiculo) {
        int disponibilidad = 0;
        for (int i = 0; i < listaVehiculosEnBox.size(); i++) {
            Vehiculo vehiculoAux = listaVehiculosEnBox.get(i);
            disponibilidad = this.cantPlazas - vehiculoAux.getPlazasQueOcupa();
            if (disponibilidad >= vehiculo.getPlazasQueOcupa()) {
                disponibilidad = disponibilidad;
            } else {
                disponibilidad = 0;
            }
        }
        return disponibilidad;
    }

    // quitar Vehículos de la Lista
    // declaro Getter y Setter
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantPlazas() {
        return cantPlazas;
    }

    public void setCantPlazas(int cantPlazas) {
        this.cantPlazas = cantPlazas;
    }

    public ArrayList<Vehiculo> getListaVehiculosEnBox() {
        return listaVehiculosEnBox;
    }

    public void setListaVehiculosEnBox(ArrayList<Vehiculo> listaVehiculosEnBox) {
        this.listaVehiculosEnBox = listaVehiculosEnBox;
    }

    // Como buena práctica se declara el toString (cuando están determinados TODOS los atributos)
    @Override
    public String toString() {
        return "Box{" + "nombre=" + nombre + ", cantPlazas=" + cantPlazas + ", listaVehiculosEnBox=" + listaVehiculosEnBox + '}';
    }

    // Como buena práctica se declaran equals (y hashCode) (cuando están determinados TODOS los atributos)    
//    @Override
//    public int hashCode() {
//        int hash = 3;
//        hash = 47 * hash + Objects.hashCode(this.nombre);
//        hash = 47 * hash + this.cantPlazas;
//        hash = 47 * hash + Objects.hashCode(this.vehiculoEnBox);
//        hash = 47 * hash + (this.estado ? 1 : 0);
//        return hash;
//    }
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Box other = (Box) obj;
//        if (this.cantPlazas != other.cantPlazas) {
//            return false;
//        }
//        if (this.estado != other.estado) {
//            return false;
//        }
//        if (!Objects.equals(this.nombre, other.nombre)) {
//            return false;
//        }
//        return Objects.equals(this.vehiculoEnBox, other.vehiculoEnBox);
//    }
}
