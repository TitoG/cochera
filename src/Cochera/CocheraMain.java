/*  Requerimientos Funcionales:
 *         Verificar Disponibilidad Cocheras             → Clase Boxes
 *         Verificar Ingreso y Egreso de Vehículos       → Clase Registro
 *         Registrar Vehiculos Ingresados: Tipo, Patente, DNI,                               → Clase Registro (tipo de datos: Vehiculo)
 *                   Horas: Cochera que se Ocupa, Ingreso, Egreso, Tiempo Real de Ocupación, → Clase Registro
 *                   Valor: Locación x Hora (x Cantidad Plazas Ocupadas),                    → Clase Cobro 
 *                          Tiempo a Cobrar, Monto Total a Cobrar                            → Clase Cobro
 *          Registrar Caja: [Caja Inicial], Gastos del Día, Ingresos del Día, Caja Final     → Clase Caja        
 *  ESCALABILIDAD:        
 *                    Horario: de 8 a 23 hs (flexible)     →     (p. ej. Día Hora Ingreso != Día Hora Egreso)
 *      Pregunta al Cliente: Las Horas nocturnas: Se cobran?, a qué Valor?
 *      Pregunta al Cliente: Alquila Cocheras por Día?, a qué Valor?
 *                    Apertura: Diaria (flexible)                    
 *                    Proyecto: Ampliar Cochera
 *   Requerimientos No Funcionales:
 *             Cambio de Estado de las Plazas de los Boxes                 → Clase Registro    (Método:      
 *             Cantidad de Plazas Ocupadas x Vehículo                      → Clase TipoVehículo    (Método: ocupPlazas)
 *             Cálculo del Tiempo de Ocupación de la Cochera x Vehículo    → Clase Registro    (Método:        )
 *             Redondeo del Tiempo de Ocupación de la Cochera x Vehículo   → Clase Registro
 *             Cálculo del Monto a Cobrar x Vehículo                       → Clase Cobro
 *             Cálculo de los Gastos del Día                               → Clase Caja    
 *             Cálculo de los Ingresos Diarios                             → Clase Caja
 *             Cálculo del Valor Final de la Caja                          → Clase Caja
 */
package Cochera;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author vhgav
 */
public class CocheraMain {

    // declaro el main
    public static void main(String[] args) {

        // Declaramos los Objetos necesarios:
        // Declaramos Tipos de Vehículo
        TipoVehiculo tipoMoto = new TipoVehiculo(1, "moto");
        TipoVehiculo tipoAuto = new TipoVehiculo(2, "auto");

        // Declaramos Lista de Boxes
        ArrayList<Box> listaBoxes = new ArrayList<>();

        // Agregamos Boxes a la Lista de Boxes
        listaBoxes.add(new Box("AB", 2));
        listaBoxes.add(new Box("CD", 2));
        listaBoxes.add(new Box("EF", 2));
        listaBoxes.add(new Box("GH", 2));
        listaBoxes.add(new Box("IJ", 2));
        listaBoxes.add(new Box("KL", 2));

        // Llamamos a ListaVehiculosEnBox
        // Declaramos un Registro
        Registro registro1 = new Registro();

        // Declaramos e inicializamos el scanner
        Scanner scan = new Scanner(System.in);

        // Menú para entrar Datos, solicitamos la opción de la Anotación a efectuar
        System.out.println("Menú Principal: ");
        System.out.println("Seleccione Anotación a efectuar");
        System.out.println("Para Registrar el Ingreso de un vehículo ingrese 1 ");
        System.out.println("Para Registrar el Egreso de un vehículo ingrese 2 ");
        System.out.println("Para Registrar un Gasto ingrese 3 ");
        System.out.println("Para Registrar un Ingreso ingrese 4 ");
        System.out.println("Para Registrar el Estado de Caja al Final del día ingrese 5 ");
        int registro = scan.nextInt();

        //  Efectuamos el Registro seleccionado
        switch (registro) {
            // Registro de Ingreso de un Vehículo (iniciar un nuevo Registro)
            case 1:
                TipoVehiculo tipoVehiculo = null;
                // Solicitamos la entrada del Tipo de Vehículo
                System.out.println("Escriba el tipo de vehículo: 1 Moto, 2 Auto");
                int tipoMovil = scan.nextInt();
                // "Limpiamos" la lectura del scaner
                scan.nextLine();
                // Asignamos valor String a la variable Tipo de Vehículo
                if (tipoMovil == 1) {
                    tipoVehiculo = tipoMoto;
                } else if (tipoMovil == 2) {
                    tipoVehiculo = tipoAuto;
                } else {
                    System.out.println("Ingreso Inválido: Escriba el tipo de vehículo: 1 Moto, 2 Auto");
                }

                // Solicitamos la entrada del Dominio del vehículo                
                System.out.println("Escriba el Dominio del Vehículo: ");
                String dominio = scan.nextLine();
                // Solicitamos la entrada del DNI del titular                 
                System.out.println("Escriba el DNI del titular: ");
                int documento = scan.nextInt();
                // "Limpiamos" la lectura del scaner                
                //scan.nextLine();
                // Instanciamos el objeto Vehículo ingresado
                Vehiculo vehiculoIngresado = new Vehiculo(dominio, documento, tipoVehiculo);
                // Buscamos box libre: "este vehiculo debe ingresar"

                for (int i = 0; i < listaBoxes.size(); i++) {
                    Box box = listaBoxes.get(i);
                    box.mostrarListaVehiculosEnBox();
                }

                // Pedimos al usuario que elija qué cochera quiere usar
                System.out.println("Elija en qué Box ingresará el nuevo Vehículo");
                System.out.println("Para el Box AB ingrese 0 ");
                System.out.println("Para el Box CD ingrese 1 ");                
                System.out.println("Para el Box EF ingrese 2 ");
                System.out.println("Para el Box GH ingrese 3 ");                
                System.out.println("Para el Box IJ ingrese 4 ");
                System.out.println("Para el Box KL ingrese 5 ");                               
                int box = scan.nextInt();
                listaBoxes.get(box).agregarVehiculoaLista(vehiculoIngresado);
                System.out.println(listaBoxes);

                break;

            //  Registro de Egreso de un Vehículo                         
            case 2:
                System.out.println("Escriba la Patente del vehículo que Egresa");
                String patente = scan.nextLine();
                break;

            case 3:
                // Solicitamos la entrada del Gasto
                System.out.println("Escriba el Gasto efectuado: ");
                float gasto = scan.nextFloat();
                break;

            case 4:
            // Solicitamos la entrada del Ingreso
//                System.out.println("Escriba el Ingreso percibido: ");
//                float gasto = scan.nextFloat();
//                break;

            // Solicitamos la Suma
            case 5:
        }

    }

//    Vehiculo auto1 = new Vehiculo("ABC123", 12345678, tipoAuto);
    Box plaza1 = new Box("AB", 2);
    //Box.boxesCochera.add(plaza1);

    // verifico Cocheras disponibles
    // (normalmente se muestra al usuario Lista de Cocheras disponibles para que elija)
    //Registro reg1 = new Registro(LocalDateTime.now(), auto1, plaza1);
}

//private static TipoVehiculo newTipoVehiculo(int i, String moto) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

/*                if (scan.nextInt() == 1) {
                    String TipoVehiculo.nombre = moto;
                    scan.next();
                } else if (scan.nextInt() == 2) {
                    String nombre = auto;
                    scan.next();
                } else {
                    System.out.println("Ingreso Inválido: Escriba el tipo de vehículo: 1 Moto, 2 Auto");
                }*/
