/* Esta Clase declara:
 *                     Atributos:  la Patente del Vehículo [dato tipo String]
 *                                 el DNI del Titular del Vehículo [dato tipo int]
 *                                 el Tipo de Vehículo del Vehículo [dato tipo TipoVehiculo]
 * 
 *                      Métodos:   Constructor vacío
 *                                 Constructor con todos los Atributos
 * 
 *                                 t1 que determina las Plazas que ocupa el Vehículo
 *                                 
 *                                 Getter y Setter
 *                                 toString()
 *                                 equals() y hashCode()                                
 */
package Cochera;

import java.util.Objects;

/**
 *
 * @author vhgav
 */

    // declaro Clase Vehiculo
public class Vehiculo {

    // declaro Atributos de la Clase
    String patente;
    int dniTitular;
    TipoVehiculo tipoVehiculo;

    // declaro Constructor vacío
    public Vehiculo() {
    }

    // declaro Constructor con todos los Atributos
    public Vehiculo(String dominio, int documento, TipoVehiculo movil) {
        this.patente = dominio;
        this.dniTitular = documento;
        this.tipoVehiculo = movil;
    }

    // Método que determina las Plazas que ocupa el vehículo → t1
    public int getPlazasQueOcupa() {
        return this.tipoVehiculo.getPlazasQueOcupa();
    }

    // declaro Getter y Setter
    public String getPatente() {
        return this.patente;
    }

    public void setPatente(String placa) {
        this.patente = placa;
    }

    public int getDniTitular() {
        return this.dniTitular;
    }

    public void setDniTitular(int documento) {
        this.dniTitular = documento;
    }

    public TipoVehiculo getTipoVehiculo() {
        return this.tipoVehiculo;
    }

    public void SetTipoVehiculo(TipoVehiculo rodado) {
        this.tipoVehiculo = rodado;
    }
    
    // Como buena práctica se declara el toString (cuando están determinados TODOS los atributos) 
    @Override
    public String toString() {
        return "Vehiculo{" + "patente=" + patente + ", dniTitular=" + dniTitular + ", tipoVehiculo=" + tipoVehiculo + '}';
    }
    
    // Como buena práctica se declaran equals (y hashCode) (cuando están determinados TODOS los atributos)    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.patente);
        hash = 47 * hash + this.dniTitular;
        hash = 47 * hash + Objects.hashCode(this.tipoVehiculo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (this.dniTitular != other.dniTitular) {
            return false;
        }
        if (!Objects.equals(this.patente, other.patente)) {
            return false;
        }
        return Objects.equals(this.tipoVehiculo, other.tipoVehiculo);
    }
}
