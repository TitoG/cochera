/* Esta Clase registra:
 *                      Atributos:  el Box que se inscribe en el Registro [dato tipo Box]
 *                                  el Vehículo que se inscribe en el Registro [dato tipo Vehiculo]
 *                                  la Hora en que Ingresa ese (this) Vehículo a la Cochera [dato tipo LocalDateTime]
 *                                  la Hora en que Egresa ese (this) Vehículo de la Cochera [dato tipo LocalDateTime]
 * 
 *                     Métodos:     Constructor vacío
 *                                  Constructor con los Atributos: horaIngreso, vehiculo, box
 * 
 * 
 *                                  v1 que determina el Tiempo de Ocupación de ese (this) Vehículo en la Cochera
 *                                  v2 que calcula el Tiempo a Cobrar (redondeando Tiempo de Ocupación) por ese (this) Vehiculo en ese (this) Box
 *                                  vv que Registra el Monto a Cobrar a ese (this) Vehiculo en ese (this) Box
 *                                  // vv que verifica los Boxes con Estado: libre
 *                                  vv que determina el Estado de Ocupación de los Boxes
 * 
 *                                  Getter y Setter 
 *                                  toString()
 *                                  equals() y hashCode()

 */
package Cochera;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 *
 * @author vhgav
 */

    // declaro Clase Registro
public class Registro {

    // declaro Atributos de la Clase
    Box box;
    Vehiculo vehiculo;
    LocalDateTime horaIngreso;
    LocalDateTime horaEgreso;
    //LocalDateTime tpoOcup; // es calculable, así se guarda

    // declaro Constructor vacío
    public Registro() {
    }

    // declaro Constructor con los Atributos: horaIngreso, vehiculo, box
    public Registro(LocalDateTime horaEntrada, Vehiculo vehiculo1, Box boxAOcupar) {
        this.horaIngreso = horaEntrada;
        this.vehiculo = vehiculo1;
        this.box = boxAOcupar;
    }

    // Método que determina el Tiempo de Ocupación de ese (this) Vehículo en la Cochera → v1   
    public long[] calcularTiempoOcupacion() {
        long minutes = ChronoUnit.MINUTES.between(horaIngreso, horaEgreso);
        long minutosReloj = minutes % 60;
        long horas = minutes / 60;
        //long dias = horas / 24;
        //long horasReloj = horas%24;

        //como conozco de antemano la cantidad de datos (3) y es un número ralativamente pequeño utilizo un Array:
        long[] lista1 = new long[2];
        // lista1[0] = dias;
        lista1[0] = horas;
        lista1[1] = minutosReloj;
        return lista1;
    }
    
    // Método que inicia el Registro de un Vehículo en la Cochera
    // solicitamos la entrada del Tipo de Vehículo

    // Método que calcula el Tiempo a Cobrar (redondeando Tiempo de Ocupación) → v2
    public int calcularTiempoaCobrar() {
        int horasaCobrar;
        long[] tiempoaCobrar = calcularTiempoOcupacion();
        if (tiempoaCobrar[1] < 6) {
            horasaCobrar = (int) tiempoaCobrar[0];
            //return (int) tiempoaCobrar[0];
        } else {
            horasaCobrar = (int) (tiempoaCobrar[0] + 1);
            //return (int) (tiempoaCobrar[0] + 1);
        }
        return horasaCobrar;
    }

    // declaro Getter y Setter    
    public Box getBox() {
        return this.box;
    }

    public void setBox(Box plaza) {
        this.box = plaza;
    }

    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }

    public void setVehiculo(Vehiculo medioLocomocion) {
        this.vehiculo = medioLocomocion;
    }

    public LocalDateTime getHoraIngreso() {
        return this.horaIngreso;
    }

    public void setHoraIngreso(LocalDateTime horaEntrada) {
        this.horaIngreso = horaEntrada;
    }

    public LocalDateTime getHoraEgreso() {
        return this.horaEgreso;
    }

    public void setHoraEgreso(LocalDateTime horaSalida) {
        this.horaEgreso = horaSalida;
    }

    // Como buena práctica se declara el toString (cuando están determinados TODOS los atributos)   
    @Override
    public String toString() {
        return "Registro{" + "box=" + box + ", vehiculo=" + vehiculo + ", horaIngreso=" + horaIngreso + ", horaEgreso=" + horaEgreso + '}';
    }
    
    // Como buena práctica se declaran equals (y hashCode) (cuando están determinados TODOS los atributos
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.box);
        hash = 67 * hash + Objects.hashCode(this.vehiculo);
        hash = 67 * hash + Objects.hashCode(this.horaIngreso);
        hash = 67 * hash + Objects.hashCode(this.horaEgreso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Registro other = (Registro) obj;
        if (!Objects.equals(this.box, other.box)) {
            return false;
        }
        if (!Objects.equals(this.vehiculo, other.vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.horaIngreso, other.horaIngreso)) {
            return false;
        }
        if (!Objects.equals(this.horaEgreso, other.horaEgreso)) {
            return false;
        }
        return true;
    }
    
}


    
    /*long minutes = ChronoUnit.MINUTES.between(fromDate, toDate);
        long hours = ChronoUnit.HOURS.between(fromDate, toDate);
        public Duration.between(horaIngreso.LocalDateTime, horaEgreso.LocalDateTime).toMillis();
        Puede convertir milis a cualquier unidad que desee:

        String.format("%d minutes %d seconds", 
          TimeUnit.MILLISECONDS.toMinutes(millis),
          TimeUnit.MILLISECONDS.toSeconds(millis) - 
          TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));*/
 
 