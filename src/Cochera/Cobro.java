/* Esta Clase registra:
 *                      Atributos:  el Valor de cada Plaza [dato tipo float]
 *                                  el Monto a Cobrar por 
 * 
 *                      Métodos:    Constructor vacío
 *                                  
 * 
 *                                  xx que Calcula y Guarda el Monto a Cobrar
 *                                  
 * 
 *                                  Getter y Setter                                  
 *                                  toString()
 *                                  equals() y hashCode()
 */
package Cochera;

/**
 *
 * @author vhgav
 */

    // declaro Clase Cobro
public class Cobro {
    
    // declaro Atributos de la Clase
    float valorPlaza;
    float montoaCobrar;
    
    // declaro Constructor Vacío
    public Cobro(){
        
    }
}
