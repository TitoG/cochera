/* Esta Clase registra:
 *                      Atributos:  el Valor Inicial (del Día) de la Caja                     
 *                                  el Valor de los Gastos del Día
 *                                  el Valor de los Ingresos del Día
 *                                  el Valor Final (del Día) de la Caja
 * 
 *                      Métodos:    Constructor vacío
 *                                  
 * 
 *                                  ww que Suma y Guarda los Gastos del Día
 *                                  ww que Suma y Guarda los Ingresos del Día
 *                                  ww que Obtiene y Guarda el Valor Final (del Día) de Caja
 * 
 *                                  Getter y Setter                                  
 *                                  toString()
 *                                  equals() y hashCode()
 */
package Cochera;

/**
 *
 * @author vhgav
 */

    // declaro Clase Caja
public class Caja {
    
    // declaro Atributos de la Clase
    float cajaInicial;
    float gastosDia;
    float ingresosDia;
    float cajaFinal;
        
    // declaro Constructor vacío
    public Caja(){        
    }

    // Método que Suma y Guarda los Gastos del Día
    public float calcularGastosDia(){
        gastosDia = 0f;
        
        return gastosDia;
    }
    
    // declaro Getter y Setter    
    public float getCajaInicial(){
        return this.cajaInicial;
    }
    
    public void setCajaInicial(float cantDineroInicial){
        this.cajaInicial = cantDineroInicial;
   }
        
    public float getGastosDia(){
        return this.gastosDia;
    }
    
    public void setGastosDia(float erogacion){
        this.gastosDia = erogacion;
    }
    
    public float getIngresosDia(){
        return this.ingresosDia;
    }
    
    public void setIngresosDia(float ingresosDelDia){
        this.ingresosDia = ingresosDelDia;
    }
    
    public float getCajaFinal(){
        return this.cajaFinal;
    }
    
    public void setCajaFinal(float cajaFinalDelDia){
        this.cajaFinal = cajaFinalDelDia;
    }
    
    // Como buena práctica se declara el toString (cuando están determinados TODOS los atributos)

    @Override
    public String toString() {
        return "Caja{" + "gasto=" + gastosDia + ", ingresosDia=" + ingresosDia + ", cajaFinal=" + cajaFinal + '}';
    }
    
    // Como buena práctica se declaran equals (y hashCode) (cuando están determinados TODOS los atributos)    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Float.floatToIntBits(this.gastosDia);
        hash = 47 * hash + Float.floatToIntBits(this.ingresosDia);
        hash = 47 * hash + Float.floatToIntBits(this.cajaFinal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Caja other = (Caja) obj;
        if (Float.floatToIntBits(this.gastosDia) != Float.floatToIntBits(other.gastosDia)) {
            return false;
        }
        if (Float.floatToIntBits(this.ingresosDia) != Float.floatToIntBits(other.ingresosDia)) {
            return false;
        }
        return Float.floatToIntBits(this.cajaFinal) == Float.floatToIntBits(other.cajaFinal);
    }
 
    
}
